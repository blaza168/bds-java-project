# Project Dockerization

1. In the project root directory, open `Git bash` and enter:
```shell
touch Dockerfile
```
this creates an file (without extension!).

2. Open the Dockerfile in the text editor (e.g., Notepad++), and add the following lines:
```dockerfile
FROM maven:3.8.3-jdk-11-slim AS build
```
This selects the Docker image (use `maven:3.8.3-jdk-11-slim` image named as build)

3. Copy the files from host machine to the Docker image (only the essential one!). Keep Docker images as small as possible!
```dockerfile
COPY /etc/application.properties /app/etc/application.properties
COPY pom.xml /app/pom.xml
COPY src /app/src
```

4. Set the workdir to `/app`
```dockerfile
WORKDIR /app
```

5. Build the project in the Docker image and copy the resulting `.jar` file into the `/app/bds-rest-java.jar`
```dockerfile
RUN mvn clean install -DskipTests && \
	cp /app/target/bds-rest-java-*.jar /app/bds-rest-java.jar
```

6. Expose the service on the `port 8082` to the `host machine`
```dockerfile
EXPOSE 8082
```

7. Set Java app as entrypoint
```dockerfile
ENTRYPOINT ["java", "-Dspring.org.but.feec.config.location=/app/etc/application.properties", "-jar", "/app/bds-rest-java.jar"]
```

Check `etc/application.properties` (check the URL for the database, we use the container name not localhost!) -- it works since the services are on the same Docker network

8. Check that it matches. The whole configuration should be as follows:
```dockerfile
FROM maven:3.8.3-jdk-11-slim AS build
COPY /etc/application.properties /app/etc/application.properties
COPY pom.xml /app/pom.xml
COPY src /app/src
WORKDIR /app
RUN mvn clean install -DskipTests && \
	cp /app/target/bds-rest-java-*.jar /app/bds-rest-java.jar
EXPOSE 8082
ENTRYPOINT ["java", "-Dspring.org.but.feec.config.location=/app/etc/application.properties", "-jar", "/app/bds-rest-java.jar"]
```

Or you can visit the following [link](https://gitlab.com/but-courses/bpc-bds/bds-rest-java/-/blob/1-1-bds-rest-dockerize-solution/Dockerfile) with one of the possible solutions.
