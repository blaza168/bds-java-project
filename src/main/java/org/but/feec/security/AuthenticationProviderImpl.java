package org.but.feec.security;

import org.but.feec.service.UserAuthService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class AuthenticationProviderImpl implements AuthenticationProvider {

    private final UserAuthService userAuthService;

    public AuthenticationProviderImpl(UserAuthService userAuthService) {
        this.userAuthService = userAuthService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
       String username = authentication.getName();
       String pwd = authentication.getCredentials().toString();

        UserDetails userDetails = userAuthService.loadUserByUsername(username);

        if (!userAuthService.passwordMatch(pwd, userDetails.getPassword())) {
            throw new BadCredentialsException("Provided credentials are not valid");
        }
        return new UsernamePasswordAuthenticationToken(username, pwd, userDetails.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
}
