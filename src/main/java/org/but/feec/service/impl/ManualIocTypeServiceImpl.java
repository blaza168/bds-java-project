package org.but.feec.service.impl;

import org.but.feec.data.entity.ManualIoc;
import org.but.feec.data.entity.ManualIocType;
import org.but.feec.data.repository.ManualIocTypeRepository;
import org.but.feec.data.repository.api.ManualIocTypeStatsDto;
import org.but.feec.exception.NotFoundException;
import org.but.feec.service.ManualIocTypeService;
import org.but.feec.service.api.UpdateManualIocTypeDto;
import org.but.feec.service.api.view.ManualIocTypeDetailView;
import org.but.feec.service.api.view.ManualIocTypeStatsView;
import org.but.feec.service.api.view.ManualIocTypeView;
import org.but.feec.service.exception.ResourceNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ManualIocTypeServiceImpl implements ManualIocTypeService {

    private final ManualIocTypeRepository manualIocTypeRepository;

    public ManualIocTypeServiceImpl(ManualIocTypeRepository manualIocTypeRepository) {
        this.manualIocTypeRepository = manualIocTypeRepository;
    }

    @Override
    public ManualIocTypeDetailView findByName(String name) {
        ManualIocType manualIocType = this.manualIocTypeRepository.findByName(name)
                .orElseThrow(() -> new NotFoundException("Ioc type was not found"));

        List<ManualIoc> manualIocList = this.manualIocTypeRepository.getIocDetails(name);

        return new ManualIocTypeDetailView(manualIocType.getName(), manualIocList);
    }

    @Override
    public ManualIocTypeView update(UpdateManualIocTypeDto updateManualIocTypeDto) {
        this.manualIocTypeRepository.update(updateManualIocTypeDto.getCurrentName(), updateManualIocTypeDto.getNewName());
        // Pull this from db (and add check) ... + add transaction
        return new ManualIocTypeView(updateManualIocTypeDto.getNewName());
    }

    @Override
    public void delete(String name) {
        ManualIocType manualIocType = this.manualIocTypeRepository.findByName(name)
                .orElseThrow(() -> new NotFoundException("Ioc type was not found"));

        this.manualIocTypeRepository.delete(name);
    }

    @Override
    public List<ManualIocTypeView> findAll() {
        List<ManualIocType> manualIocTypes = this.manualIocTypeRepository.findAll();
        // Use mapper ... don't pull ID when not needed anywhere ...
        return manualIocTypes.stream()
                .map((manualIocType -> new ManualIocTypeView(manualIocType.getName())))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public ManualIocTypeView storeIocType(String type) {
        this.manualIocTypeRepository.store(type);
        // Pull this from db, validate. With transaction
        return new ManualIocTypeView(type);
    }

    @Override
    public ManualIocTypeStatsView getStats(String type) {
        Optional<ManualIocTypeStatsDto> statsOpt = this.manualIocTypeRepository.getStats(type);
        ManualIocTypeStatsDto stats = statsOpt.orElseThrow(() ->
                new ResourceNotFoundException("Ioc type cannot be found"));

        return new ManualIocTypeStatsView(type, stats.getUsage());
    }
}
