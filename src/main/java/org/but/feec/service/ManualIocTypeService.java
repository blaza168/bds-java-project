package org.but.feec.service;

import org.but.feec.service.api.UpdateManualIocTypeDto;
import org.but.feec.service.api.view.ManualIocTypeDetailView;
import org.but.feec.service.api.view.ManualIocTypeStatsView;
import org.but.feec.service.api.view.ManualIocTypeView;
import java.util.List;

public interface ManualIocTypeService {
    ManualIocTypeDetailView findByName(String name);
    ManualIocTypeView update(UpdateManualIocTypeDto updateManualIocTypeDto);
    void delete(String name);
    List<ManualIocTypeView> findAll();
    ManualIocTypeView storeIocType(String type);
    ManualIocTypeStatsView getStats(String type);
}
