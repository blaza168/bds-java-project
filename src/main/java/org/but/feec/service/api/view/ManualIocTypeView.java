package org.but.feec.service.api.view;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ManualIocTypeView {
    private final String name;
}
