package org.but.feec.service.api.view;

import com.google.common.collect.ImmutableList;
import lombok.Getter;
import lombok.ToString;
import org.but.feec.data.entity.ManualIoc;
import java.util.List;

@Getter
@ToString
public class ManualIocTypeDetailView {
    private final String manualIocType;
    private final ImmutableList<ManualIoc> manualIocs;

    public ManualIocTypeDetailView(String manualIocType, List<ManualIoc> manualIocList) {
        this.manualIocType = manualIocType;
        this.manualIocs = manualIocList == null ? ImmutableList.of() : ImmutableList.copyOf(manualIocList);
    }
}
