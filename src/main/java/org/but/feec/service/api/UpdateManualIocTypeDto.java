package org.but.feec.service.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class UpdateManualIocTypeDto {
    private final String currentName;
    private final String newName;
}
