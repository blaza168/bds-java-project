package org.but.feec.service.api.view;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ManualIocTypeStatsView {
    private final String iocType;
    private final Long usageCount;
}
