package org.but.feec.service;

import org.but.feec.data.entity.UserAccount;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserAuthService extends UserDetailsService {
    boolean passwordMatch(String passwd, String hash);
}
