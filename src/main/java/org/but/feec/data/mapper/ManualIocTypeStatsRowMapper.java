package org.but.feec.data.mapper;

import org.but.feec.data.repository.api.ManualIocTypeStatsDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class ManualIocTypeStatsRowMapper implements RowMapper<ManualIocTypeStatsDto> {
    @Override
    public ManualIocTypeStatsDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new ManualIocTypeStatsDto(
                rs.getLong("usageCount")
        );
    }
}
