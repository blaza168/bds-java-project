package org.but.feec.data.mapper;

import org.but.feec.data.entity.ManualIocType;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class ManualIocTypeRowMapper implements RowMapper<ManualIocType> {
    @Override
    public ManualIocType mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new ManualIocType(
                rs.getLong("id_manual_ioc_type"),
                rs.getString("name")
        );
    }
}
