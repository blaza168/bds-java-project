package org.but.feec.data.mapper;

import org.but.feec.data.entity.ManualIoc;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class ManualIocRowMapper implements RowMapper<ManualIoc> {
    @Override
    public ManualIoc mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new ManualIoc(rs.getLong("id_manual_ioc"),
                rs.getLong("id_manual_ioc_type"),
                rs.getString("value"));
    }
}
