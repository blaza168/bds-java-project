package org.but.feec.data.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class ManualIocType {
    private final Long id;
    private final String name;
}
