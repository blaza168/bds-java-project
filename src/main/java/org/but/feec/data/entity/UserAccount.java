package org.but.feec.data.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import java.io.Serializable;

@Getter
@AllArgsConstructor
@ToString
public class UserAccount implements Serializable {
    private final Long id;
    private final String username;
    private final String password;
}
