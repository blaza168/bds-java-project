package org.but.feec.data.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class ManualIoc {
    private final Long id;
    private final Long idManualIocType;
    private final String value;
}
