package org.but.feec.data.repository.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class ManualIocTypeStatsDto {
    private final Long usage;
}
