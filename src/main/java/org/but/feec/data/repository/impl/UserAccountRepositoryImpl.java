package org.but.feec.data.repository.impl;

import org.but.feec.data.entity.UserAccount;
import org.but.feec.data.mapper.UserAccountRowMapper;
import org.but.feec.data.repository.UserAccountRepository;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.Optional;

@Component
public class UserAccountRepositoryImpl implements UserAccountRepository {

    private final JdbcTemplate jdbc;

    public UserAccountRepositoryImpl(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Optional<UserAccount> findByUsername(String username) {
        UserAccount account = jdbc.queryForObject("SELECT * FROM bds.user_account WHERE username = ?",
                new UserAccountRowMapper(), username);

        return Optional.ofNullable(account);
    }
}
