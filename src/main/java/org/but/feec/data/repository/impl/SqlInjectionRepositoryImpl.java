package org.but.feec.data.repository.impl;

import org.but.feec.data.repository.SqlInjectionRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SqlInjectionRepositoryImpl implements SqlInjectionRepository {

    private final JdbcTemplate jdbc;

    public SqlInjectionRepositoryImpl(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public List<String> execute(String text) {
        return jdbc.queryForList("SELECT email FROM bds.sql_injection_table1 WHERE email = '" + text + "'", String.class);
    }
}
