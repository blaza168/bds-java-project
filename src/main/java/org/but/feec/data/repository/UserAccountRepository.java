package org.but.feec.data.repository;

import org.but.feec.data.entity.UserAccount;

import java.util.Optional;

public interface UserAccountRepository  {

    Optional<UserAccount> findByUsername(String username);

}
