package org.but.feec.data.repository;

import org.but.feec.data.entity.ManualIoc;
import org.but.feec.data.entity.ManualIocType;
import org.but.feec.data.repository.api.ManualIocTypeStatsDto;
import java.util.Optional;
import java.util.List;

public interface ManualIocTypeRepository {
    Optional<ManualIocType> findByName(String name);
    List<ManualIocType> findAll();
    void delete(String name);
    void update(String current, String newName);
    void store(String name);
    Optional<ManualIocTypeStatsDto> getStats(String name);
    List<ManualIoc> getIocDetails(String type); // RENAME!!!
}
