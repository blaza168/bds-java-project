package org.but.feec.data.repository;

import java.util.List;

public interface SqlInjectionRepository {
    List<String> execute(String text);
}
