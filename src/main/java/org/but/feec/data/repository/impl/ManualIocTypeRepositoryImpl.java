package org.but.feec.data.repository.impl;

import lombok.extern.slf4j.Slf4j;
import org.but.feec.data.entity.ManualIoc;
import org.but.feec.data.entity.ManualIocType;
import org.but.feec.data.mapper.ManualIocRowMapper;
import org.but.feec.data.mapper.ManualIocTypeRowMapper;
import org.but.feec.data.mapper.ManualIocTypeStatsRowMapper;
import org.but.feec.data.repository.ManualIocTypeRepository;
import org.but.feec.data.repository.api.ManualIocTypeStatsDto;
import org.but.feec.service.api.UpdateManualIocTypeDto;
import org.but.feec.service.exception.ResourceNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class ManualIocTypeRepositoryImpl implements ManualIocTypeRepository {

    private final JdbcTemplate jdbc;

    public ManualIocTypeRepositoryImpl(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    @Override
    public Optional<ManualIocType> findByName(String name) {
        try {
            ManualIocType manualIocType = jdbc.queryForObject("SELECT * FROM bds.manual_ioc_type WHERE name = ?",
                    new ManualIocTypeRowMapper(), name);
            return Optional.ofNullable(manualIocType);
        } catch (EmptyResultDataAccessException e) {
            log.warn("findByName: Empty result for name " + name, e);
            throw new ResourceNotFoundException("ManualIocType was not found.");
        }
    }

    @Override
    public List<ManualIocType> findAll() {
        return jdbc.query("SELECT * FROM bds.manuaL_ioc_type", new ManualIocTypeRowMapper());
    }

    @Override
    public void delete(String name) {
        jdbc.update("DELETE FROM bds.manual_ioc_type WHERE name = ?", name);
    }

    @Override
    public void update(String current, String newName) {
        jdbc.update("UPDATE bds.manual_ioc_type SET name = ? WHERE name = ?",
                newName, current);
    }

    @Override
    public void store(String name) {
        jdbc.update("INSERT INTO bds.manual_ioc_type (name) VALUES (?)", name);
    }

    @Override
    public Optional<ManualIocTypeStatsDto> getStats(String name) {
        try {
            Long id = jdbc.queryForObject("SELECT id_manual_ioc_type FROM bds.manual_ioc_type WHERE name = ?",
                    Long.class, name);
            ManualIocTypeStatsDto stats = jdbc.queryForObject("SELECT COUNT(id_manual_ioc) as usageCount FROM bds.manual_ioc " +
                    "WHERE id_manual_ioc_type = ?", new ManualIocTypeStatsRowMapper(), id);
            return Optional.ofNullable(stats);
        } catch (EmptyResultDataAccessException e) {
            log.warn("GetStats: Empty result for name " + name, e);
            throw new ResourceNotFoundException("ManualIocType was not found.");
        }
    }

    @Override
    public List<ManualIoc> getIocDetails(String type) {
        try {
            return jdbc.query("SELECT manual_ioc.* " +
                            "FROM bds.manual_ioc " +
                            "INNER JOIN bds.manual_ioc_type ON (manual_ioc.id_manual_ioc_type = manual_ioc_type.id_manual_ioc_type) " +
                            "WHERE manual_ioc_type.name = ?",
                    new ManualIocRowMapper(), type);
        } catch (EmptyResultDataAccessException e) {
            log.warn("getIocDetails: Empty result for name " + type, e);
            throw new ResourceNotFoundException("ManualIocType was not found.");
        }
    }
}
