package org.but.feec.rest.exceptionhandling;

import lombok.extern.slf4j.Slf4j;
import org.but.feec.service.exception.ResourceNotFoundException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.util.UrlPathHelper;
import javax.servlet.http.HttpServletRequest;
import java.time.Clock;
import java.time.LocalDateTime;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
@Slf4j
public class CustomRestExceptionHandler {

    private final static UrlPathHelper URL_PATH_HELPER = new UrlPathHelper();

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ExceptionHandler({ResourceNotFoundException.class})
    public ApiError handlerResourceNotFoundException(ResourceNotFoundException ex, HttpServletRequest req) {
        log.error("handlerResourceNotFoundException: " + ex.getMessage(), ex);

        ApiError apiError = new ApiError();
        apiError.setMessage(ex.getMessage());
        apiError.setStatus(HttpStatus.NOT_FOUND);
        apiError.setTimestamp(LocalDateTime.now(Clock.systemUTC()));
        apiError.setPath(URL_PATH_HELPER.getRequestUri(req));
        return apiError;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler({BadCredentialsException.class})
    public ApiError handleBadCredentialsException(BadCredentialsException ex, WebRequest request) {
        log.error("handleBadCredentialsException: " + ex.getMessage(), ex);
        ApiError apiError = new ApiError();
        apiError.setMessage(ex.getMessage());
        apiError.setStatus(HttpStatus.UNAUTHORIZED);
        apiError.setTimestamp(LocalDateTime.now(Clock.systemUTC()));
        return apiError;
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ApiError handleInternalError(Exception ex, HttpServletRequest req) {
        log.error("handleInternalError: " + ex.getMessage(), ex);
        ApiError apiError = new ApiError();
        apiError.setMessage(ex.getMessage());
        apiError.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        apiError.setTimestamp(LocalDateTime.now(Clock.systemUTC()));
        apiError.setPath(URL_PATH_HELPER.getRequestUri(req));
        return apiError;
    }
}
