package org.but.feec.rest.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;

@Getter
public class UpdateManualIocTypeCommand {
    @Length(min = 2, max = 15)
    private final String currentName;
    @Length(min = 2, max = 15)
    private final String newName;

    public UpdateManualIocTypeCommand(
            @JsonProperty("currentName") String currentName,
            @JsonProperty("newName") String newName
    ) {
        this.currentName = currentName;
        this.newName = newName;
    }
}
