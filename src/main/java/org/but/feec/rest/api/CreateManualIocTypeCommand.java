package org.but.feec.rest.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotNull;

@Getter
public class CreateManualIocTypeCommand  {

    @Length(min = 2, max=10)
    @NotNull
    private String type;

    public CreateManualIocTypeCommand(@JsonProperty("type") String type) {
        this.type = type;
    }

}
