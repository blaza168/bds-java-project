package org.but.feec.rest;

import org.but.feec.rest.api.CreateManualIocTypeCommand;
import org.but.feec.rest.api.UpdateManualIocTypeCommand;
import org.but.feec.service.ManualIocTypeService;
import org.but.feec.service.api.UpdateManualIocTypeDto;
import org.but.feec.service.api.view.ManualIocTypeDetailView;
import org.but.feec.service.api.view.ManualIocTypeStatsView;
import org.but.feec.service.api.view.ManualIocTypeView;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/manual-ioc-types")
public class ManualIocTypeController {

    private final ManualIocTypeService manualIocTypeService;

    public ManualIocTypeController(ManualIocTypeService manualIocTypeService) {
        this.manualIocTypeService = manualIocTypeService;
    }

    @GetMapping
    public List<ManualIocTypeView> findAll() {
        return this.manualIocTypeService.findAll();
    }

    @GetMapping("/{name}")
    public ManualIocTypeDetailView findByName(@PathVariable("name") String name) {
        return this.manualIocTypeService.findByName(name);
    }

    @GetMapping("/stats/{name}")
    public ManualIocTypeStatsView getStats(@PathVariable("name") String name) {
        return this.manualIocTypeService.getStats(name);
    }

    @PostMapping
    public ManualIocTypeView createIocType(
            @RequestBody @Valid CreateManualIocTypeCommand command
    ) {
        return this.manualIocTypeService.storeIocType(command.getType());
    }

    @PutMapping
    public ManualIocTypeView updateIocType(
            @RequestBody @Valid UpdateManualIocTypeCommand command
    ) {
        UpdateManualIocTypeDto dto = new UpdateManualIocTypeDto(command.getCurrentName(), command.getNewName());
        return this.manualIocTypeService.update(dto);
    }

    @DeleteMapping("/{name}")
    public void deleteIocType(@PathVariable("name") String name) {
        this.manualIocTypeService.delete(name);
    }
}
