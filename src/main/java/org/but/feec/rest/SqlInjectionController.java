package org.but.feec.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.but.feec.data.repository.SqlInjectionRepository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/sqli")
public class SqlInjectionController {

    @Getter
    private static final class SqlInjectionPayload {
            private final String payload;

            public SqlInjectionPayload(
                    @JsonProperty("payload") String payload
            ) {
                this.payload = payload;
            }
    }

    private final SqlInjectionRepository sqlInjectionRepository;

    public SqlInjectionController(SqlInjectionRepository sqlInjectionRepository) {
        this.sqlInjectionRepository = sqlInjectionRepository;
    }

    @PostMapping
    public List<String> doIt(
            @RequestBody SqlInjectionPayload payload
    ) {
        return sqlInjectionRepository.execute(payload.getPayload());
    }
}
