package org.but.feec.config;

import org.but.feec.data.repository.UserAccountRepository;
import org.but.feec.service.UserAuthService;
import org.but.feec.service.impl.UserAuthServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class ServiceConfiguration {
    @Bean
    public UserAuthService userAuthService(PasswordEncoder passwordEncoder, UserAccountRepository userAccountRepository) {
        return new UserAuthServiceImpl(userAccountRepository, passwordEncoder);
    }
}
